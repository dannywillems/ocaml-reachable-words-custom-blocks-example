# Example for Obj.reachable_words on custom blocks

Example project to show Obj.reachable_words does not take in account the size of the elements pointed by custom blocks.

## Setup

```
opam switch create ./ 4.12.0
opam install ocamlformat.0.19.0 merlin dune.2.9
```

## Execute

```
dune ./src/test.exe
```

Output is:
```
Size for foo256: 3
Size for foo512: 3
Size for bytes256: 6
Size for bytes512: 10
```
