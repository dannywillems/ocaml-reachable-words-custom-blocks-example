#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <stdlib.h>

typedef struct foo256_s {
  unsigned char t[256];
} foo256_t;

#define Foo256_val(v) (*((foo256_t **)Data_custom_val(v)))

static void finalize_free_foo256(value v) { free(Foo256_val(v)); }

static struct custom_operations foo256_ops = {"foo256_t",
                                              finalize_free_foo256,
                                              custom_compare_default,
                                              custom_hash_default,
                                              custom_serialize_default,
                                              custom_deserialize_default,
                                              custom_compare_ext_default,
                                              custom_fixed_length_default};

typedef struct foo512_s {
  unsigned char t[512];
} foo512_t;

CAMLprim value caml_allocate_foo256_stubs(value unit) {
  CAMLparam1(unit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&foo256_ops, sizeof(foo256_t *), 0, 1);
  void *p = calloc(1, sizeof(foo256_t));
  if (p == NULL)
    caml_raise_out_of_memory();
  foo256_t **d = (foo256_t **)Data_custom_val(block);
  *d = p;
  CAMLreturn(block);
}

#define Foo512_val(v) (*((foo512_t **)Data_custom_val(v)))

static void finalize_free_foo512(value v) { free(Foo512_val(v)); }

static struct custom_operations foo512_ops = {"foo512_t",
                                              finalize_free_foo512,
                                              custom_compare_default,
                                              custom_hash_default,
                                              custom_serialize_default,
                                              custom_deserialize_default,
                                              custom_compare_ext_default,
                                              custom_fixed_length_default};

CAMLprim value caml_allocate_foo512_stubs(value unit) {
  CAMLparam1(unit);
  CAMLlocal1(block);
  block = caml_alloc_custom(&foo512_ops, sizeof(foo512_t *), 0, 1);
  void *p = calloc(1, sizeof(foo512_t));
  if (p == NULL)
    caml_raise_out_of_memory();
  foo512_t **d = (foo512_t **)Data_custom_val(block);
  *d = p;
  CAMLreturn(block);
}
