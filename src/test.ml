module Stubs = struct
  type foo256

  external allocate_foo256 : unit -> foo256 = "caml_allocate_foo256_stubs"

  type foo512

  external allocate_foo512 : unit -> foo512 = "caml_allocate_foo512_stubs"
end

let () =
  let foo256 = Stubs.allocate_foo256 () in
  let foo512 = Stubs.allocate_foo512 () in
  let bytes256 = Bytes.create 32 in
  let bytes512 = Bytes.create 64 in
  Printf.printf "Size for foo256: %d\n" Obj.(reachable_words (magic foo256)) ;
  Printf.printf "Size for foo512: %d\n" Obj.(reachable_words (magic foo512)) ;
  Printf.printf "Size for bytes256: %d\n" Obj.(reachable_words (magic bytes256)) ;
  Printf.printf "Size for bytes512: %d\n" Obj.(reachable_words (magic bytes512))
